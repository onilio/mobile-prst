// Export pages
export '/pages/user_info/user_info_widget.dart' show UserInfoWidget;
export '/pages/falta_destalhes/falta_destalhes_widget.dart'
    show FaltaDestalhesWidget;
export '/pages/propina_lista/propina_lista_widget.dart' show PropinaListaWidget;
export '/pages/us_info/us_info_widget.dart' show UsInfoWidget;
export '/pages/profile_user/profile_user_widget.dart' show ProfileUserWidget;
export '/pages/user_card/user_card_widget.dart' show UserCardWidget;
export '/pages/calendario/calendario_widget.dart' show CalendarioWidget;
export '/pages/home/home_widget.dart' show HomeWidget;
export '/pages/bannerpage_login/bannerpage_login_widget.dart'
    show BannerpageLoginWidget;
export '/pages/login/login_widget.dart' show LoginWidget;
export '/pages/falta_lista/falta_lista_widget.dart' show FaltaListaWidget;
export '/pages/horario_list/horario_list_widget.dart' show HorarioListWidget;
export '/pages/file_page/file_page_widget.dart' show FilePageWidget;
export '/pages/video_player/video_player_widget.dart' show VideoPlayerWidget;
export '/pages/notfound/notfound_widget.dart' show NotfoundWidget;
