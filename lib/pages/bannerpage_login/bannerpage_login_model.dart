import '/flutter_flow/flutter_flow_util.dart';
import 'bannerpage_login_widget.dart' show BannerpageLoginWidget;
import 'package:flutter/material.dart';

class BannerpageLoginModel extends FlutterFlowModel<BannerpageLoginWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
