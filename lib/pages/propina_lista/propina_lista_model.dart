import '/flutter_flow/flutter_flow_util.dart';
import 'propina_lista_widget.dart' show PropinaListaWidget;
import 'package:flutter/material.dart';

class PropinaListaModel extends FlutterFlowModel<PropinaListaWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
