import '/flutter_flow/flutter_flow_util.dart';
import 'user_info_widget.dart' show UserInfoWidget;
import 'package:flutter/material.dart';

class UserInfoModel extends FlutterFlowModel<UserInfoWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
