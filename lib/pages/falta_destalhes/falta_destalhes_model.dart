import '/flutter_flow/flutter_flow_util.dart';
import 'falta_destalhes_widget.dart' show FaltaDestalhesWidget;
import 'package:flutter/material.dart';

class FaltaDestalhesModel extends FlutterFlowModel<FaltaDestalhesWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
