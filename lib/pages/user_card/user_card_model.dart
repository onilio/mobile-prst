import '/flutter_flow/flutter_flow_util.dart';
import 'user_card_widget.dart' show UserCardWidget;
import 'package:flutter/material.dart';

class UserCardModel extends FlutterFlowModel<UserCardWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
