import '/flutter_flow/flutter_flow_util.dart';
import 'us_info_widget.dart' show UsInfoWidget;
import 'package:flutter/material.dart';

class UsInfoModel extends FlutterFlowModel<UsInfoWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
