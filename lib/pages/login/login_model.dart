import '/flutter_flow/flutter_flow_util.dart';
import 'login_widget.dart' show LoginWidget;
import 'package:flutter/material.dart';

class LoginModel extends FlutterFlowModel<LoginWidget> {
  String? _errorMessage;

  final unfocusNode = FocusNode();
  // State field(s) for emailAddress widget.
  FocusNode? emailAddressFocusNode;
  TextEditingController? emailAddressController;
  String? Function(BuildContext, String?)? emailAddressControllerValidator;
  // State field(s) for password widget.
  FocusNode? passwordFocusNode;
  TextEditingController? passwordController;
  late bool passwordVisibility;
  String? Function(BuildContext, String?)? passwordControllerValidator;
  
  @override
  void initState(BuildContext context) {
    passwordVisibility = false;
  }

  @override
  void dispose() {
    unfocusNode.dispose();
    emailAddressFocusNode?.dispose();
    emailAddressController?.dispose();

    passwordFocusNode?.dispose();
    passwordController?.dispose();
  }

 /*void _login() async {
    
    final String email = _emailController.text;
    final String password = _passwordController.text;

    setState(() {
      _errorMessage = password;
    });

    // Basic validation
    if (email.isEmpty || password.isEmpty) {
      
      setState(() {
        _errorMessage = 'Username ou password vazio!';
      });
      //debugPrintWarning('This is a warning message.');
      return;
    }else{
      context.pushNamed('Home');
    }

    /*try {
      final statusCode = await auth(email, password, context);
      if (statusCode == 200) {
        // Authentication successful
        setState(() {
          _errorMessage = null;
        });

        context.pushNamed('Home');
      } else {
        // Authentication failed
        setState(() {
          _errorMessage = 'Username / email ou senha inválida.';
        });
      }
    } catch (e) {
      // Handle any errors that occurred during authentication
      setState(() {
        _errorMessage = 'Erro no sistema.';
      });
    }*/
  }*/
}
