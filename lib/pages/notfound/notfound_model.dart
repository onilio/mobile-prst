import '/flutter_flow/flutter_flow_util.dart';
import 'notfound_widget.dart' show NotfoundWidget;
import 'package:flutter/material.dart';

class NotfoundModel extends FlutterFlowModel<NotfoundWidget> {
  ///  State fields for stateful widgets in this page.

  final unfocusNode = FocusNode();

  @override
  void initState(BuildContext context) {}

  @override
  void dispose() {
    unfocusNode.dispose();
  }
}
