import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;
import 'package:device_info/device_info.dart';
import 'package:wifi_info_flutter/wifi_info_flutter.dart';
import 'package:device_imei/device_imei.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<int> auth(String email, String password, BuildContext context) async {
  try {
    String deviceInfo = 'android';
    String macAddress = '00-B0-D0-63-C2-26';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    
    final Uri uri = Uri.parse('http://127.0.0.1/api/v0.1/mobile/$deviceInfo/$macAddress/login/${base64.encode(utf8.encode(email))}/${base64.encode(utf8.encode(password))}');
    
    final response = await http.get(uri, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });

    if (response.statusCode == 200) {
      Map<String, dynamic> data = jsonDecode(response.body);
      
      if (data['message'] == 200) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Conectado com sucesso. Configurando o sistema...'),
        ));
        //print(data['user_info']['profile_img']);

        if (await prefs.setString('device', jsonEncode(data['device'])) && await prefs.setString('user', jsonEncode(data['user_info']))) {
          //print(jsonDecode(prefs.getString('user') ?? ''));
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Aplicativo configurado. Seja Bem-Vindo!'),
          ));
          return data['message'];
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(data['sms']),
        ));
        return -1;
      }
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Erro ao logar!!'),
      ));
      return -1;
    } else {
      throw Exception('Falha ao autenticar');
    }
  } catch (e) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Erro: $e'),
    ));
    return -1;
  }
}

Future<dynamic> getDeviceInfo(BuildContext context) async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  if (Theme.of(context).platform == TargetPlatform.android) {
    return await deviceInfo.androidInfo;
  } else if (Theme.of(context).platform == TargetPlatform.iOS) {
    return await deviceInfo.iosInfo;
  }
  return null;
}
